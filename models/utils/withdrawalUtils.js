'use strict';
const generateReceipt = require('./generateReceiptUtils');

const withdrawal = async (body) => {
  let amount = Number(body.amount);

    if (amount % 500 === 0) {
        if (messageFromBank.amount < amount) {
          return [400, 'Insufficient Funds']
        }
        else {
          messageFromBank.amount = messageFromBank.amount - amount;
          let receipt =  await generateReceipt(amount.toString(), 'withdrawal', messageFromBank.account, messageFromBank.amount);
          return [200, 'Please take your money', receipt];
        }
    }
    else {
      return [400, 'Amount must be multiples of 500 and 1000']
    }
};

module.exports = withdrawal;
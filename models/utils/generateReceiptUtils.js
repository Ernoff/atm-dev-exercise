'use strict'

let 
  Receipt = require('../Receipts');

const generateReceipt = (amount, type, account, ending_balance) => {
    amount = String(amount);
    account = String(account);
    ending_balance = String(ending_balance);
    type = String(type);

  let newReceipt = new Receipt({amount, type_of_transaction: type, account, ending_balance});
  return newReceipt.save()
    .then(doc => {
      return doc;
    })
    .catch(error => {
      return 'Unable to provide receipt';
    })
}

module.exports = generateReceipt;

module.exports = function () {
  let 
    year = new Date().getFullYear().toString(),
    month = new Date().getMonth().toString(),
    day = new Date().getDate().toString(),
    hour = new Date().getHours().toString(),
    minutes = new Date().getMinutes().toString();

  if (month.length === 1) {
    month = "0"+ month;
  }
  if (day.length === 1) {
    day = "0"+ day;
  }
  if (hour.length === 1) {
    hour = "0"+ hour;
  }
  if (minutes.length === 1) {
    minutes = "0"+ minutes;
  }
  return `${year}${month}${day}.${hour}${minutes}`; 
}
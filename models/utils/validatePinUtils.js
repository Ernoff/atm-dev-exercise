'use strict'

const validatePin = (req, res, next) => {
  if (req.body.pin) {
    if (req.body.pin === messageFromBank.pin) {
      next();
    }
    else {
      if (messageFromBank.count >= 2) {
        return res.status(400).json({message: 'Card seized. Please contact your Institution'});
      }
      else {
        messageFromBank.count ++;
        return res.status(400).json({message: 'Wrong secret key'});
      }
    }
  }
  else {
    return res.status(409).json({message: 'A secret pin is required'});
  }
}

module.exports = validatePin;
'use strict';
const generateReceipt = require('./generateReceiptUtils');

const checkBalance = async (body) => {
  if(body.account_type === messageFromBank.type) {
    let receipt =  await generateReceipt('', 'check_balance', messageFromBank.account, messageFromBank.amount);
    return [200, `Your balance is ${messageFromBank.amount}`, receipt];
  }
  else {
    return [400, 'No balance was found for the account' ];
  }
}

module.exports = checkBalance
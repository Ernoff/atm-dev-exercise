const
  mongoose = require('mongoose'),
  setDateTimeKey = require('./utils/dateUtils'),
  Schema = mongoose.Schema;

const ReceiptSchema = Schema({
  amount: {
    type: String,
  },
  location: {
    type: String,
    default: 'Position X'
  },
  type_of_transaction: {
    type: String
  },
  account: {
    type: String
  },
  ending_balance: {
    type: String
  },
  created_at: {
    type: String,
    default: setDateTimeKey 
  }
});

const ReceiptModel = mongoose.model('receipts', ReceiptSchema);
module.exports = ReceiptModel;
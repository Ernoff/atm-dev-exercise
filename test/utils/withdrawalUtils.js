'use strict';

const 
  assert = require('assert'),
  {withdrawalUtils} = require('../../models/utils');

describe('Withdrawal Service', () => {


  it('should return an array of length 3',  async () => {
    let previous_amount = global.messageFromBank.amount;
    let response =  await withdrawalUtils({amount: 5000});
    let later_amount = global.messageFromBank.amount;
    assert.equal(response[0], 200, 'Returns 200 success code');
    assert(response[2]._id, 'Id of saved receipt found')
    assert.equal(response.length, 3);
    assert(previous_amount > later_amount);
  });


  it('should return an array of length 2',  async () => {
    let response =  await withdrawalUtils({amount: 5001});
    assert.equal(response[0], 400, 'Returns 400 success code');
    assert.equal(response.length, 2);
  });

  it('should return an array of length 2',  async () => {
    let response =  await withdrawalUtils({amount: 15000});
    assert.equal(response[0], 400, 'Returns 400 success code');
    assert.equal(response.length, 2);
  });

  after(function () {
    process.exit(0);
  });
});
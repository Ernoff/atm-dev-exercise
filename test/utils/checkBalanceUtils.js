'use strict';

const 
  assert = require('assert'),
  {checkBalanceUtils} = require('../../models/utils');

describe('Check_Balance Service', () => {

  it('should return an array of length 3',  async () => {
    let response =  await checkBalanceUtils({account_type: "savings"});
    assert.equal(response[0], 200, 'Returns 200 success code');
    assert(response[2]._id, 'Id of saved receipt found')
    assert.equal(response.length, 3);
  });


  it('should return an array of length 2',  async () => {
    let response =  await checkBalanceUtils({account_type: "current"});
    assert.equal(response[0], 400, 'Returns 200 success code');
    assert.equal(response.length, 2);
  });
  
 
});
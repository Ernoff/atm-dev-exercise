'use strict';

const 
  assert = require('assert'),
  app = require('../../app'),
  http = require('http'),
  request = require('supertest'),
  server = http.createServer(app);


describe('Routes', () => {
  before(function () {
    server.listen(8000);
  });


  it('atm/welcome should return a 200 response',  (done) => {
    request(server)
    .get('/atm/welcome')
    .expect(200)
    .end((err, res) => {
      if(err) done.fail();
      done();
    });
  });

  it('atm/exit should return a 200 response',  (done) => {
    request(server)
    .post('/atm/exit')
    .expect(200)
    .end((err, res) => {
      if(err) done.fail();
      done();
    });
  });

  it('atm/cancel should return a 200 response',  (done) => {
    request(server)
    .post('/atm/cancel')
    .expect(200)
    .end((err, res) => {
      if(err) done.fail();
      done();
    });
  });

  it('atm/services should return a 200 response',  (done) => {
    request(server)
    .get('/atm/services')
    .expect(200)
    .end((err, res) => {
      if(err) done.fail();
      assert(res.body.services.length > 2);
      done();
    });
  });

  it('atm/check_balance should return a 200 response',  (done) => {
    request(server)
    .post('/atm/check_balance')
    .send({
      "pin": "0000",
      "account_type": "savings",
      "card_no": "12345678900987654321"
    })
    .set('Accept', 'application/json')
    .expect(200)
    .end((err, res) => {
      if(err) done.fail();
      done();
    });
  });

  it('atm/withdrawal should return wrong secret key',  (done) => {
    request(server)
    .post('/atm/withdrawal')
    .send({
      "amount": 5000,
      "pin": "0010",
      "account_type": "savings",
      "card_no": "12345678900987654321"
    })
    .set('Accept', 'application/json')
    .expect(400)
    .end((err, res) => {
      // console.log(err, res)
      if(err) done.fail();
      assert(res.body.message, 'Wrong secret key')
      done();
    });
  });

  it('atm/withdrawal should return error due to false card',  (done) => {
    request(server)
    .post('/atm/withdrawal')
    .send({
      "amount": 5000,
      "pin": "0000",
      "account_type": "savings",
      "card_no": "12345678900987654320"
    })
    .set('Accept', 'application/json')
    .expect(400)
    .end((err, res) => {
      // console.log(err, res)
      if(err) done.fail();
      assert(res.body.message.includes('Contact your institution'))
      done();
    });
  });

  
});
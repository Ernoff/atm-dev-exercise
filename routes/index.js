'use strict';
//=============================================================================
/**
 * Module dependencies
 */
//=============================================================================
const
  express = require('express'),
  { withdrawalUtils, checkBalanceUtils, validatePinUtils, 
    servicesUtils, checkCardDetailsUtils } = require('../models/utils');

const router = express.Router();

let code, message, receipt, services;


/**
* @api {get} /atm/welcome ATM API Test
* @apiName Welcome
* @apiGroup Atm 
* @apiSuccess {String} message Message from Server
* 
*/

//used to test atm api service
router.get('/welcome', (req, res) => {
  return res.send({message: 'Welcome please enter your secret number'});
});


/**
* @api {get} /atm/services ATM available services
* @apiName Services
* @apiGroup Atm
* @apiSuccess {String} message Message from Server
* @apiSuccess {Array} services Array of available services on the ATM API
*  
*/

//used to get available services
router.get('/services', async (req, res) => {
  [code, message, services] = await servicesUtils();
  return res.status(code).json({message, services});
});


/**
* @api {post} /atm/withdrawal Withdrawal Service
* @apiName Withdrawal
* @apiGroup Atm
* @apiSuccess {String} message Message from Server
* @apiSuccess {Object} receipt Receipt of transaction performed
*
* @apiError {String} message Message from server
*/
// handle withdrawal
router.post('/withdrawal', checkCardDetailsUtils, validatePinUtils, async (req, res) => {
  [code, message, receipt] = await withdrawalUtils(req.body);
  if (receipt) {
    return res.status(code).json({message, receipt});
  }
  return res.status(code).json({message});
})



/**
* @api {post} /atm/check_balance Check Balance Service
* @apiName Check Balance
* @apiGroup Atm
* @apiSuccess {String} message Message from Server
* @apiSuccess {Object} receipt Receipt of transaction performed
*
* @apiError {String} message Message from server
*/
//check balance
router.post('/check_balance', checkCardDetailsUtils, validatePinUtils, async (req, res) => {
  [code, message, receipt] = await checkBalanceUtils(req.body);
  if (receipt) {
    return res.status(code).json({message, receipt});
  }
  return res.status(code).json({message});
})


/**
* @api {post} /atm/exit End Transaction 
* @apiName Exit
* @apiGroup Atm
* @apiSuccess {String} message Message from Server
*
*/
//end operations
router.post('/exit', (req, res) => {
  messageFromBank = {
    pin: '0000',
    amount: 10000,
    type: 'savings',
    count: 0,
    account: '0123456789',
    card_no: '12345678900987654321',
  };
  return res.status(200).json({message: 'Thank you. Please take your card'});
})

/**
* @api {post} /atm/cancel Cancel Transaction 
* @apiName Cancel
* @apiGroup Atm
* @apiSuccess {String} message Message from Server
*
*/
//cancel operations
router.post('/cancel', (req, res) => {
  messageFromBank = {
    pin: '0000',
    amount: 10000,
    type: 'savings',
    count: 0,
    account: '0123456789',
    card_no: '12345678900987654321',
  };
  return res.status(200).json({message: 'Operation has been cancelled'});
})

module.exports = router;
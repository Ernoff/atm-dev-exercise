'use strict';

if (process.env.NODE_ENV != 'production') {
    require('dotenv').config();
}
/**
 * Import Module Dependencies
 */
//=============================================================================
const
  express = require('express'),
  bParser = require('body-parser'),
  path = require('path'),
  mongoose = require('mongoose'),
  cors = require('cors'),
  helmet = require('helmet');
//=============================================================================
/**
 * Create Express App
 */
//=============================================================================
const app = express();

//=============================================================================
/**
 * Module variables
 */
//=============================================================================
const
  port = process.env.PORT,
  env = process.env.NODE_ENV,
  dBURL = process.env.dBURL,
  atmRoutes = require('./routes');
  
let
  db;
//=============================================================================
/**
 * App config and settings
 */
//=============================================================================
app.disable('x-powered-by');
app.set('port', port);
app.set('env', env);
//=============================================================================
/**
 * dBase connection
 */
//=============================================================================
mongoose.connect(dBURL, { useNewUrlParser: true });
db = mongoose.connection;
db.on('error', function (err) {
  console.error('There was a db connection error');
  return  console.error(err.message);
});
db.once('connected', function () {
  return console.log('Successfully connected to ' + dBURL);
});
db.once('disconnected', function () {
  return console.error('Successfully disconnected from ' + dBURL);
});
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.error('dBase connection closed due to app termination');
    return process.exit(0);
  });
});
//=============================================================================
/**
 * Middleware Stack
 */
//=============================================================================
app.use(helmet.hsts({
  maxAge: 5000,
  includeSubDomains: true
}));
app.use(helmet.frameguard({
  action: 'sameorigin'
}));
app.use(helmet.xssFilter());
app.use(helmet.noSniff());
app.use(cors());
app.use(bParser.json());
app.use(bParser.urlencoded({extended: true}));
app.engine('html', require('ejs').renderFile);
app.use(express.static(path.join(__dirname, '/')));
app.set('apidoc', path.join(__dirname, 'apidoc'));
app.set('view engine', 'html');
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    if (req.method === 'OPTIONS') {
      res.status(200).end();
    }
    else {
      next();
    }
});

//mock message from bank
global.messageFromBank = {
  pin: '0000',
  amount: 10000,
  type: 'savings',
  account: '0123456789',
  card_no: '12345678900987654321',
  count: 0
};
//=============================================================================
/**
 * Routes
 */
//=============================================================================


app.use('/atm', atmRoutes);

app.get('/apidoc', function(req, res) {
  res.render("index")
});


//=============================================================================
/**
 * Export Module
 */
//=============================================================================
module.exports = app;
//=============================================================================
# atm-dev-exercise #

This is an api microservice for an ATM Machine powered by Nodejs and Express

### Things to Note

* To mock response from a bank server, there exist a single global object that initates response for a single account
* An env.example has been included. Edit it to `.env` and fill your development variables
* To generate documentation run
  - `npm i -g apidoc`
  - `cd routes/ && apidoc -i . -o ../apidoc/ && cd ../`

### How to Run

* Clone the repository
* `CD` into the repository
* Run `npm install`

### Key Commands

* To start dev server - `npm run dev`
* To run test suites - `npm run test`
* To check test coverage - `npm run coverage`

### Links
* An API documentation detailing all the routes once generated, can be found at `http://localhost:${PORT}/apidoc`

### Contact Dev
[Ernest Offiong](ernest.offiong@gmail.com)